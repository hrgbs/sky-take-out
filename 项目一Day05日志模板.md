# 项目一Day05日志
### 1. Redis是什么?相比Mysql有什么特点?

```
Redis是一个基于 内存 的key-value结构数据库。Redis 是互联网技术领域使用最为广泛的存储中间件。
基于内存存储，读写性能高
适合存储热点数据（热点商品、资讯、新闻）
企业应用广泛
```



### 2. Redis有哪几种常见数据类型,分别有什么特点?

```
字符串(string)：普通字符串，Redis中最简单的数据类型
哈希(hash)：也叫散列，类似于Java中的HashMap结构
列表(list)：按照插入顺序排序，可以有重复元素，类似于Java中的LinkedList
集合(set)：无序集合，没有重复元素，类似于Java中的HashSet
有序集合(sorted set/zset)：集合中每个元素关联一个分数(score)，根据分数升序排序，没有重复元素
```



### 3. Redis五种常见数据类型的常用命令有哪些?分别是什么作用?

```
Redis 中字符串类型常用命令：
SET key value 设置指定 key 的值
GET key 获取指定 key 的值
SETEX key seconds value 设置指定 key 的值，并将 key 的过期时间设为 seconds 秒
SETNX key value 只有在 key 不存在时设置 key 的值

Redis hash 是一个 string 类型的 field 和 value 的映射表，hash 特别适合用于存储对象，常用命令：
HSET key field value 将哈希表 key 中的字段 field 的值设为 value
HGET key field 获取存储在哈希表中指定字段的值
HDEL key field 删除存储在哈希表中的指定字段
HKEYS key 获取哈希表中所有字段
HVALS key 获取哈希表中所有值
HGETALL key 获取在哈希表中指定 key 的所有字段和值

Redis 列表是简单的字符串列表，按照插入顺序排序，常用命令：
先插入的在尾部，后插入的在头部
LPUSH key value1 [value2] 将一个或多个值插入到列表头部
LRANGE key start stop 获取列表指定范围内的元素
RPOP key 移除并获取列表最后一个元素
LLEN key 获取列表长度
BRPOP key1 [key2 ] timeout 移出并获取列表的最后一个元素， 如果列表没有元素会阻塞列表直到等待超 时或发现可弹出元素为止

Redis set 是 string 类型的无序集合。集合成员是唯一的，这就意味着集合中不能出现重复的数据，常用命令：
SADD key member1 [member2] 向集合添加一个或多个成员
SMEMBERS key 返回集合中的所有成员
SCARD key 获取集合的成员数
SINTER key1 [key2] 返回给定所有集合的交集
SUNION key1 [key2] 返回所有给定集合的并集
SDIFF key1 [key2] 返回给定所有集合的差集
SREM key member1 [member2] 移除集合中一个或多个成员

sorted set 操作命令：
ZADD key score1 member1 [score2 member2] 向有序集合添加一个或多个成员，或者更新已存在成员的 分数
ZRANGE key start stop [WITHSCORES] 通过索引区间返回有序集合中指定区间内的成员
ZINCRBY key increment member 有序集合中对指定成员的分数加上增量 increment
ZREM key member [member ...] 移除有序集合中的一个或多个成员

通用命令：
KEYS pattern 查找所有符合给定模式( pattern)的 key
EXISTS key 检查给定 key 是否存在
TYPE key 返回 key 所储存的值的类型
TTL key 返回给定 key 的剩余生存时间(TTL, time to live)，以秒为单位
DEL key 该命令用于在 key 存在是删除 key

```



### 4. 店铺营业状态如何设置和修改?

```
营业状态数据存储方式：基于Redis的字符串来进行存储，约定： 1表示营业 0表示打烊
在sky-server模块中，创建ShopController,定义setStatus设置营业状态---设置营业状态
定义创建ShopController定义getStatus接口查询营业状态方法---管理端查询营业状态
定义创建com.sky.controller.user包，并创建ShopController，定义getStatus接口查询营业状态方法---用户端查询营业状态
```