package com.sky.test;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

//@SpringBootTest
public class HttpClientTest2 {
    @Test
    public void  testPOST ()throws Exception{
        //1.创建HttpClient客户端对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //2.创建HttpPost请求对象，指定请求路径
        HttpPost httpPost = new HttpPost("http://localhost:8080/admin/employee/login");
        //3.封装请求体对象，设置到Post请求对象中
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username","admin");
        jsonObject.put("password","123456");
        StringEntity entity =new StringEntity(jsonObject.toString());
        httpPost.setEntity(entity);
        //指定请求编码方式
        entity.setContentEncoding("utf-8");
        //数据格式
        entity.setContentType("application/json");
        httpPost.setEntity(entity);

        //4.发送请求，获取响应对象
        CloseableHttpResponse response = httpClient.execute(httpPost);
        //5.获取响应状态码
        int statusCode = response.getStatusLine().getStatusCode();
        System.err.println("statusCode:"+statusCode);
        //6.获取响应体
        HttpEntity responseBody= response.getEntity();
        String str = EntityUtils.toString(responseBody);
        System.err.println("responseBody:"+str);
        //6.释放资源
        response.close();
        httpClient.close();
    }
}
