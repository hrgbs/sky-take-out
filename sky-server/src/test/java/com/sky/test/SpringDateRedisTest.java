package com.sky.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.*;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

//@SpringBootTest
public class SpringDateRedisTest {
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void testRedisTemplate() {
        System.out.println(redisTemplate);
        //string 数据操作
        ValueOperations valueOperations = redisTemplate.opsForValue();
        //hash类型的数据操作
        HashOperations hashOperations = redisTemplate.opsForHash();
        //list类型的数据操作
        ListOperations listOperations = redisTemplate.opsForList();
        //set类型数据操作
        SetOperations setOperations = redisTemplate.opsForSet();
        //zset类型数据操作
        ZSetOperations zSetOperations = redisTemplate.opsForZSet();

    }

    /**
     * 操作字符串类型的数据
     */
    @Test
    public void testString() {
        //  set   get setex  setnx
        redisTemplate.opsForValue().set("city", "北京");
        String city = (String) redisTemplate.opsForValue().get("city");
        System.out.println(city);
        redisTemplate.opsForValue().set("code", "1234", 3, TimeUnit.MINUTES);
        redisTemplate.opsForValue().setIfAbsent("lock", "1");
        redisTemplate.opsForValue().setIfAbsent("lock", "2");
    }

    /**
     * 操作hash类型数据
     */
    @Test
    public void testHash() {
        //hset hget hdel hkeys havls
        HashOperations hashOperations = redisTemplate.opsForHash();

        //hset的用法
        hashOperations.put("100", "name", "tom");
        hashOperations.put("100", "age", "20");
        //hget的用法
        String name = (String) hashOperations.get("100", "name");
        System.out.println(name);

        //hkeys  获取所有hashkey
        Set keys = hashOperations.keys("100");
        System.out.println(keys);

        //hvals 获取所有的value
        List values = hashOperations.values("100");
        System.out.println(values);

        //hdel的用法 删除
        hashOperations.delete("100", "age");

    }

    /**
     * 操作列表类型的数据
     */
    @Test
    public void testList() {
        // lpush  lrange rpop  llen
        ListOperations listOperations = redisTemplate.opsForList();

        //从左侧插入数据
        listOperations.leftPushAll("mylist", "a", "b", "c");
        listOperations.leftPush("mylist", "d");

        //查看整个列表
        List mylist = listOperations.range("mylist", 0, 1);
        System.out.println(mylist);

        //从列表右侧移除数据
        listOperations.rightPop("mylist");

        //获取列表的长度
        Long size = listOperations.size("mylist");
        System.out.println(size);
    }

    /**
     * 操作集合类型的数据
     */
    @Test
    public void testSet() {
        // sadd smembers scards sinter sunion srem
        SetOperations setOperations = redisTemplate.opsForSet();

        // 向集合中插入元素
        setOperations.add("set1", "a", "b", "c", "d");
        setOperations.add("set2", "a", "b", "x", "y");

        // 获取集合中的元素
        Set members = setOperations.members("set1");
        System.out.println(members);
        //获取集合大小
        Long size = setOperations.size("set1");
        System.out.println(size);

        //   计算集合的交集
        Set intersect = setOperations.intersect("set1", "set2");
        System.out.println(intersect);

        //计算集合的并集
        Set union = setOperations.union("set1", "set2");
        System.out.println(union);


        //删除集合的元素
        setOperations.remove("set1", "a", "b");

    }

    /**
     * 操作有序集合类型的数据
     */
    @Test
    public void testZset() {
        //zadd  zrange zincrby zrem
        ZSetOperations zSetOperations = redisTemplate.opsForZSet();
        //向集合添加元素
        zSetOperations.add("zset1", "a", 10);
        zSetOperations.add("zset1", "b", 12);
        zSetOperations.add("zset1", "c", 9);

        //查看所有的元素
        Set zset1 = zSetOperations.range("zset1", 0, -1);
        System.out.println(zset1);

        //为集合中指定的元素加10
        zSetOperations.incrementScore("zset1", "c", 10);

        //删除集合中的元素
        zSetOperations.remove("zset", "a", "b");

    }

    /**
     * 通用命令操作
     */
    @Test
    public void testCommon() {
        // keys exists type del
        //获得所有的key
        Set keys = redisTemplate.keys("*");
        System.out.println(keys);
       //判断是否存在
        Boolean name = redisTemplate.hasKey("name");
        Boolean set1 = redisTemplate.hasKey("set1");

        //查看所有key查看类型
        for (Object key : keys) {
            DataType type = redisTemplate.type(key);
            System.out.println(type.name());
        }
        //删除mylist
         redisTemplate.delete("mylist");
    }
}
