package com.sky.test;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;

public class POITest1 {
    public static void main(String[] args) throws Exception {
        //使用IO`关联要读取的Excel文件
        FileInputStream fis = new FileInputStream("E:/桌面/5.27截图/a.xlsx");
        //创建工作簿对象，指定要读取的文件的IO流
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        //获取表格Sheet
        XSSFSheet sheet = wb.getSheetAt(0);
        for (int i=1; i<8;i++) {
            //获取行（Row）
            XSSFRow row = sheet.getRow(i);
            //获取单元格（cell）
            XSSFCell cell_2_2 = row.getCell(1);
            XSSFCell cell_2_3 = row.getCell(2);
            XSSFCell cell_2_4 = row.getCell(3);

            //获取单元格数据
            String name = cell_2_2.getStringCellValue();
            String element = cell_2_3.getStringCellValue();
            double age = cell_2_4.getNumericCellValue();
            System.out.println(name  + "," + element + "," + age);
        }


        //释放资源
        fis.close();
        wb.close();



    }
}
