package com.sky.test;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;

/**
 * 使用写excel
 */
public class POITest {
    public static void main(String[] args) throws Exception {


        //新建Excel文档（工作簿）
        XSSFWorkbook wb = new XSSFWorkbook();
        //新建一个表格Sheet
        XSSFSheet sheet = wb.createSheet("表格1");
        //新建行（Row）
        XSSFRow row0 = sheet.createRow(0);
        //新建单元格（cell）
        XSSFCell cell = row0.createCell(0);
        //写数据
        cell.setCellValue("序号");
        //将整个Excel数据从内存中写到硬盘文件中
        FileOutputStream fos = new FileOutputStream("E:\\桌面\\5.27截图\\a.xlsx");
        wb.write(fos);

        //释放资源
        fos.close();
        wb.close();



    }
}
