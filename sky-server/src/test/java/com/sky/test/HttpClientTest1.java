package com.sky.test;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;

//@SpringBootTest
public class HttpClientTest1 {
    @Test
    public void  testGET() throws Exception{
        //1.创建HttpClient客户端对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //2.创建HttpGet请求对象，指定请求路径
        HttpGet httpGet = new HttpGet("http://localhost:8080/user/shop/status");

        //3.发送请求，获取响应对象
        CloseableHttpResponse response = httpClient.execute(httpGet);
        //4.获取响应状态码
        int statusCode = response.getStatusLine().getStatusCode();
        System.err.println("statusCode:"+statusCode);
        //5.获取响应体
        HttpEntity responseBody= response.getEntity();
        String str = EntityUtils.toString(responseBody);
        System.err.println("responseBody:"+str);
        //6.释放资源
        response.close();
        httpClient.close();
    }
}
