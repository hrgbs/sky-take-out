package com.sky.controller.admin;


import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/setmeal")
@Api(tags = "套餐相关接口")
@Slf4j
public class SetmealController {
    @Autowired
    private SetmealService setmealService;

    /**
     * 新增套餐
     *
     * @param //参数
     * @return
     */
    @PostMapping
    @ApiOperation("新增套餐")
    @CacheEvict(cacheNames = "setmealCache",key = "#setmealDTO.categoryId")
    public Result save(@RequestBody SetmealDTO setmealDTO) {
        log.info("新增套餐");
        setmealService.saveWithDishes(setmealDTO);
        return Result.success();
    }

    /**
     * 套餐分页查询
     *
     * @param setmealPageQueryDTO
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("套餐分页查询")
    public Result<PageResult> page(SetmealPageQueryDTO setmealPageQueryDTO) {
        PageResult pageResult = setmealService.pageQuery(setmealPageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 套餐修改
     *
     * @return
     */
    @PutMapping
    @ApiOperation("套餐修改")
    @CacheEvict(cacheNames = "setmealCache", allEntries = true) //清理所有缓存
    public Result update(@RequestBody SetmealDTO setmealDTO) {
        setmealService.updateWithDishs(setmealDTO);
        return Result.success();
    }

    /**
     * 起售或禁售
     *
     * @param status
     * @param id
     * @return
     */
    @PostMapping("/status/{status}")
    @ApiOperation("起售或禁售")
    @CacheEvict(cacheNames = "setmealCache" , allEntries = true)//清理所有缓存
    public Result startOrStop(@PathVariable("status") Integer status, Long id) {
        setmealService.startOrStop(status, id);
        return Result.success();

    }

    /**
     * @return 根据id查询套餐
     */
    @GetMapping("/{id}")
    @ApiOperation("根据id查询套餐")
    public Result<SetmealVO> getById(@PathVariable("id") Long id) {
        SetmealVO setmealVO = setmealService.getById(id);
        return Result.success(setmealVO);

    }

    /**
     * 根据ids删除套餐
     * @param ids
     * @return
     */
    @DeleteMapping
    @ApiOperation("根据ids删除套餐")
    @CacheEvict(cacheNames = "setmealCache", allEntries = true) //清理所有缓存
    public Result delete(@RequestParam List<Long> ids) {
        setmealService.deleteBatch(ids);

        return Result.success();
    }


}
