package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;


@Mapper
public interface EmployeeMapper {

    /**
     * 根据用户名查询员工
     * @param username
     * @return
     */
    @Select("select * from employee where username= #{username}")
    Employee getByUsername(String username);


    /**
     * 增加员工信息到数据库
     * @param employee
     */
    @Insert("insert into employee(name, username, password, phone, sex, id_number, create_time, update_time, create_user, update_user, salt) "+
    "values (#{name},#{username},#{password},#{phone},#{sex},#{idNumber},#{createTime},#{updateTime},#{createUser},#{updateUser},#{salt})")
    @AutoFill(value= OperationType.INSERT)  //AOP拦截注解
    void insertEmp(Employee employee);

    /**
     * 分页查询
     * @param employeePageQueryDTO
     * @return
     */

    Page<Employee> pageQuery(EmployeePageQueryDTO employeePageQueryDTO);



    /**
     * 根据id查询员工信息
     * @param id
     * @return
     */
    @Select("select * from employee where id=#{id}")
    Employee getById(long id);

    /**
     * 根据主键动态修改属性
     * @param employee
     */
    @AutoFill(value= OperationType.UPDATE)  //AOP拦截注解
    void update(Employee employee);


    /**
     * 修改密码
     * @param password
     */
@Update("update employee set password=#{password} where id=#{currentId}")
    void editPassword(Long currentId,String password);
}
