package com.sky.mapper;

import com.sky.entity.SetmealDish;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealDishMapper {
    /**
     * 根据菜品ids查询套餐ids
     * @param dishIds
     * @return
     */
      List<Long> getSetealIdsByDishIds(List<Long>dishIds);

    /**
     * 添加套餐内的菜品信息
     * @param setmealDishes
     */

    void insertBatch(List<SetmealDish> setmealDishes);


    /**
     * 根据id删除setmealdish表
     * @param setmealId
     */
    @Delete("delete from setmeal_dish where setmeal_id = #{setmealId}")
    void deleteBySetmealId(Long setmealId);

    /**
     * 根据setmealId查询菜品
     * @param setmealId
     * @return
     */
    @Select("select * from setmeal_dish where setmeal_id = #{setmealId}")
    List<SetmealDish> getBySetmealId(Long setmealId);

    /**
     * 根据菜品id查询套餐ids
     * @param id
     * @return
     */
    @Select("select  setmeal_id from setmeal_dish where dish_id = #{id}")
    List<Long> getSetealIdsByDishId(Long id);
}
