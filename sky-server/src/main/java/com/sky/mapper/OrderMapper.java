package com.sky.mapper;

import com.sky.dto.GoodsSalesDTO;
import com.sky.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrderMapper {

    /**
     * 插入订单信息
     * @param orders
     */
    void insert(Orders orders);

    /**
     * 根据订单号和用户id查询订单
     * @param orderNumber
     * @param userId
     */
    @Select("select * from orders where number = #{orderNumber} and user_id= #{userId}")
    Orders getByNumberAndUserId(String orderNumber, Long userId);

    /**
     * 修改订单信息
     * @param orders
     */
    void update(Orders orders);


    /**
     * 根据订单状态和下单时间查询订单
     * @param status
     * @param orderTime
     * @return
     */
     @Select("select * from orders where status = #{status} and  order_time < #{orderTime}")
     List<Orders> getByStatusAndOrderTimeLT(Integer status , LocalDateTime orderTime);

    /**
     * 根据id查询订单详情
     * @param id
     * @return
     */
    @Select("select * from orders where id = #{id} ")
    Orders getById(Long id);

    /**
     * 订单统计
     * @param date
     * @param o
     * @return
     */
    Integer countByDateAndStatus(LocalDate date, Object o);

    /**
     * 销量统计
     * @param begin
     * @param end
     * @return
     */
    List<GoodsSalesDTO> sumSalesByOrderTime(LocalDate begin, LocalDate end);

    /**
     * 动态条件统计数量
     * @param map
     * @return
     */
    Integer countByMap(Map map);

    /**
     *动态条件统计营业额
     * @param map
     * @return
     */
    Double sumByMap(Map map);
}
