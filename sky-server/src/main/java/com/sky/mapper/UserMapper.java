package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.util.Map;

@Mapper
public interface UserMapper {
    /**
     * 根据openid查询用户
     * @param openid
     * @return
     */

    @Select("select * from user where openid = #{openid}")
    User getByOpenid(String openid);


    /**
     * 添加用户信息
     * @param user
     */
    void insert(User user);

    @Select("select * from user where id =#{userId}")
    User getById(Long userId);


    @Select("select count(*) from user where date_format(create_time,'%Y-%m-%d')=#{date}")
    Integer countByCreateTime(LocalDate date);

    @Select("select count(*) from user where date_format(create_time,'%Y-%m-%d')<=#{date}")
    Integer countByCreateTimeBefore(LocalDate date);

    /**
     * 动态条件统计新增用户数量
     * @param map
     * @return
     */
    Integer countByMap(Map map);
}
