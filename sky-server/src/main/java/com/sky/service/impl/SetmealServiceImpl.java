package com.sky.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.exception.SetmealEnableFailedException;
import com.sky.exception.StatusEditException;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;
    @Autowired
    private DishMapper dishMapper;


    /**
     * 新增套餐
     *
     * @param setmealDTO
     */
    @Transactional
    public void saveWithDishes(SetmealDTO setmealDTO) {
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        if (setmealDishes != null && setmealDishes.size() > 0) {
            setmealDishes.forEach(setmealDish -> {
                Long dishId = setmealDish.getDishId();
                Dish dish = dishMapper.getById(dishId);
                if (dish.getStatus() == 0) {
                    throw new SetmealEnableFailedException(MessageConstant.SETMEAL_ENABLE_FAILED);
                }
            });
            Setmeal setmeal = new Setmeal();
            BeanUtils.copyProperties(setmealDTO, setmeal);
            setmealMapper.insert(setmeal);
            //从新加入的套餐里面拿到主键id
            Long setmealId = setmeal.getId();

            //拿到套餐里面的菜品的集合
            //将主键id与菜品关联起来
            if (setmealDishes != null && setmealDishes.size() > 0) {
                setmealDishes.forEach(setmealDish -> {
                    setmealDish.setSetmealId(setmealId);
                });
                setmealDishMapper.insertBatch(setmealDishes);
            }
        }
    }


    /**
     * 套餐分页查询
     *
     * @param setmealPageQueryDTO
     * @return
     */
    public PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO) {
        PageHelper.startPage(setmealPageQueryDTO.getPage(), setmealPageQueryDTO.getPageSize());
        Page<SetmealVO> page = setmealMapper.pageQuery(setmealPageQueryDTO);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 修改套餐表信息
     * @param setmealDTO
     */
    @Transactional
    public void updateWithDishs(SetmealDTO setmealDTO) {
        //修改套餐表信息
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO, setmeal);
        setmealMapper.update(setmeal);

        //修改套餐表内的菜品信息需要先删除原套餐内的菜品表再重新添加
        Long setmealId = setmealDTO.getId();
        setmealDishMapper.deleteBySetmealId(setmealId);
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        //遍历传入的菜品表集合然后将主键setmealId关联起来
        if (setmealDishes != null && setmealDishes.size() > 0) {
            setmealDishes.forEach(setmealDish -> {
                setmealDish.setSetmealId(setmealId);
            });
        }
        setmealDishMapper.insertBatch(setmealDishes);
        // todo 查询bug

    }



    /**
     * 启用或禁用
     *
     * @param status
     * @param id
     */
    @Transactional
    public void startOrStop(Integer status, Long id) {
        // 判断菜品是否为停售 如果菜品为停售应该抛出异常
        // 根据套餐id查询菜品status
            List<Integer> dishsStatus = dishMapper.getBySetmealId(id);
        for (Integer dishstatus : dishsStatus) {
            if (dishstatus == StatusConstant.DISABLE){
                throw new StatusEditException(MessageConstant.SETMEAL_ENABLE_FAILED);
            }
        }

        Setmeal setmeal = Setmeal.builder()
                .id(id)
                .status(status)
                .build();
        setmealMapper.update(setmeal);
    }



    /**
     * 根据id查询套餐
     *
     * @param id
     */
    public SetmealVO getById(Long id) {
        Setmeal setmeal = setmealMapper.getById(id);
        List<SetmealDish> setmealDishes = setmealDishMapper.getBySetmealId(id);

        SetmealVO setmealVO = new SetmealVO();

        BeanUtils.copyProperties(setmeal, setmealVO);
        setmealVO.setSetmealDishes(setmealDishes);
        return setmealVO;
    }

    /**
     * 删除套餐
     * @param ids
     */

    @Transactional
    public void deleteBatch(List<Long> ids) {
        //判断是否为停售
        for (Long id : ids) {
        Setmeal setmeal= setmealMapper.getById(id);
        if (setmeal.getStatus() == StatusConstant.ENABLE){
            throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
        }
        }

        for (Long id : ids) {
            //删除关联的setmeal_dish表
            setmealDishMapper.deleteBySetmealId(id);
            //删除setmeal
            setmealMapper.deleteById(id);

        }

    }

    /**
     * 条件查询
     * @param setmeal
     * @return
     */
    public List<Setmeal> list(Setmeal setmeal) {
        List<Setmeal> list = setmealMapper.list(setmeal);
        return list;
    }

    /**
     * 根据id查询菜品选项
     * @param id
     * @return
     */
    public List<DishItemVO> getDishItemById(Long id) {

        return setmealMapper.getDishItemBySetmealId(id);
    }





}
