package com.sky.service.impl;

import com.sky.dto.GoodsSalesDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import com.sky.mapper.ReportMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.service.WorkspaceService;
import com.sky.vo.*;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReportServiceimpl implements ReportService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private ReportMapper reportMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private WorkspaceService workspaceService;

    /**
     *  营业额数据统计
     */
    public TurnoverReportVO turnoverStatistics(LocalDate begin, LocalDate end) {

        // 1. 获取日期列表的字符串  2023-6-1    ---   2023-6-5
        // 1.1 创建集合，用于存储每一天的日期对象
        List<LocalDate> dateList = new ArrayList<>();
        // 1.2 从开始日期遍历到结束日期，将每一天的日期对象存储到集合中。
        while (begin.compareTo(end) <= 0) {
            dateList.add(begin);
            begin = begin.plusDays(1);
        }

        // 2. 获取营业额列表的字符串
        // 2.1 创建集合，用于存储每一天的营业额
        List<Double> amountList = new ArrayList<>();
        // 2.2 遍历日期，获取每一天日期，根据日期查询对应的营业额
        for (LocalDate date : dateList) {
            Double amountSum = reportMapper.sumByDateAndStatus(date, Orders.COMPLETED);
            amountList.add(amountSum == null ? 0.0 : amountSum);
        }

        // 3. 封装vo，返回
        TurnoverReportVO turnoverReportVO = new TurnoverReportVO();
        turnoverReportVO.setDateList(StringUtils.join(dateList, ","));
        turnoverReportVO.setTurnoverList(StringUtils.join(amountList, ","));
        return turnoverReportVO;

    }

    /**
     * 统计新增用户数和用户总量
     */
    public UserReportVO userStatistics(LocalDate begin, LocalDate end) {
        // 1. 获取日期列表的字符串  2023-6-1    ---   2023-6-5
        // 1.1 创建集合，用于存储每一天的日期对象
        List<LocalDate> dateList = new ArrayList<>();
        // 1.2 从开始日期遍历到结束日期，将每一天的日期对象存储到集合中。
        while (begin.compareTo(end) <= 0) {
            dateList.add(begin);
            begin = begin.plusDays(1);
        }

        // 2. 获取每天新增用户数
        // 3. 获取用户总数
        List<Integer> newUserList = new ArrayList<>();
        List<Integer> totalUserList = new ArrayList<>();  // 2023-06-01  2023-06-05
        for (LocalDate date : dateList) {
            Integer newCount = userMapper.countByCreateTime(date);
            newUserList.add(newCount);
            Integer totalCount = userMapper.countByCreateTimeBefore(date);
            totalUserList.add(totalCount);
        }

        // 4. 封装vo，返回
        UserReportVO userReportVO = new UserReportVO();
        userReportVO.setDateList(StringUtils.join(dateList, ","));
        userReportVO.setNewUserList(StringUtils.join(newUserList, ","));
        userReportVO.setTotalUserList(StringUtils.join(totalUserList, ","));
        return userReportVO;

    }

    /**
     * 订单统计接口
     */
    @Override
    public OrderReportVO ordersStatistics(LocalDate begin, LocalDate end) {
        // 1. 获取日期列表的字符串  2023-6-1    ---   2023-6-5
        // 1.1 创建集合，用于存储每一天的日期对象
        List<LocalDate> dateList = getLocalDates(begin, end);

        // 2. 获取每天对应的订单数量
        List<Integer> orderCountList = new ArrayList<>();
        // 3. 获取每天对应的有效订单数量
        List<Integer> validOrderCountList = new ArrayList<>();
        // 定义变量统计订单总数
        Integer orderCountSum = 0;
        // 定义变量统计有效订单总数
        Integer validOrderCountSum = 0;

        for (LocalDate date : dateList) {
            Integer orderCount = orderMapper.countByDateAndStatus(date, null); // 查询当天的所有订单，不需要指定状态
            if (orderCount == null) {
                orderCount = 0;
            }
            orderCountList.add(orderCount);
            orderCountSum += orderCount; // 订单累加求和

            Integer validOrderCount = orderMapper.countByDateAndStatus(date, Orders.COMPLETED);// 查询当天的有效订单，指定状态为已完成
            if (validOrderCount == null) {
                validOrderCount = 0;
            }
            validOrderCountList.add(validOrderCount);
            validOrderCountSum += validOrderCount; // 有效订单累加求和
        }

        // 封装VO，并返回
        OrderReportVO orderReportVO = new OrderReportVO();
        orderReportVO.setDateList(StringUtils.join(dateList, ","));
        orderReportVO.setOrderCountList(StringUtils.join(orderCountList, ","));
        orderReportVO.setValidOrderCountList(StringUtils.join(validOrderCountList, ","));
        orderReportVO.setTotalOrderCount(orderCountSum);
        orderReportVO.setValidOrderCount(validOrderCountSum);
        if (orderCountSum == 0) {
            orderReportVO.setOrderCompletionRate(0.0);
        } else {
            orderReportVO.setOrderCompletionRate(validOrderCountSum.doubleValue()/orderCountSum);
        }

        return orderReportVO;
    }

    private List<LocalDate> getLocalDates(LocalDate begin, LocalDate end) {
        List<LocalDate> dateList = new ArrayList<>();
        // 1.2 从开始日期遍历到结束日期，将每一天的日期对象存储到集合中。
        while (begin.compareTo(end) <= 0) {
            dateList.add(begin);
            begin = begin.plusDays(1);
        }
        return dateList;
    }

    /**
     * 销量排行top10
     */
    @Override
    public SalesTop10ReportVO top10(LocalDate begin, LocalDate end) {
        // 1. 查询指定时间范围内的菜品和对应的销量
        List<GoodsSalesDTO> goodsSalesDTOList =  orderMapper.sumSalesByOrderTime(begin, end);
        // 2. 定义集合，存储菜品列表
        List<String> nameList = new ArrayList<>();
        // 3. 定义集合，存储销量列表
        List<Integer> numberList = new ArrayList<>();
        // 4. 遍历集合，获取销量和菜品数据，设置到对应的容器中
        for (GoodsSalesDTO goodsSalesDTO : goodsSalesDTOList) {
            nameList.add(goodsSalesDTO.getName());
            numberList.add(goodsSalesDTO.getNumber());
        }
        // 5. 封装VO，返回
        SalesTop10ReportVO salesTop10ReportVO = new SalesTop10ReportVO();
        salesTop10ReportVO.setNameList(StringUtils.join(nameList,","));
        salesTop10ReportVO.setNumberList(StringUtils.join(numberList,","));
        return salesTop10ReportVO;
    }

    /**
     * 导出运营数据报表
     */
    @Override
    @ApiOperation("导出运营数据报表")
    public XSSFWorkbook exportReport()  {
        // 1. 读取运营数据报表模板
        XSSFWorkbook wb = null;
        try {
            InputStream is = ClassLoader.getSystemResourceAsStream("template/运营数据报表模板.xlsx");
            wb = null;


            wb = new XSSFWorkbook(is);

            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 2. 调用工作台接口，查询对应的报表数据，写数据到Excel中
        LocalDate begin = LocalDate.now().plusDays(-30);
        LocalDate end = LocalDate.now().plusDays(-1);
        BusinessDataVO businessData = workspaceService.getBusinessData(LocalDateTime.of(begin, LocalTime.MIN),
                LocalDateTime.of(end, LocalTime.MAX));

        // 2.1 获取表格
        XSSFSheet sheet1 = wb.getSheetAt(0);
        // 2.2 获取行
        XSSFRow row2 = sheet1.getRow(1); // 第2行
        XSSFRow row4 = sheet1.getRow(3); // 第4行
        XSSFRow row5 = sheet1.getRow(4); // 第5行
        // 2.3 获取单元格
        XSSFCell cell_2_2 = row2.getCell(1);// 第2行第2个单元格 (日期范围)
        XSSFCell cell_4_3 = row4.getCell(2);// 第4行第3个单元格 (营业额)
        XSSFCell cell_4_5 = row4.getCell(4);// 第4行第5个单元格 (订单完成率)
        XSSFCell cell_4_7 = row4.getCell(6);// 第4行第7个单元格 (新增用户数)
        XSSFCell cell_5_3 = row5.getCell(2);// 第5行第3个单元格 (有效订单)
        XSSFCell cell_5_5 = row5.getCell(4);// 第5行第5个单元格 (平均客单价)
        // 2.4 填充数据
        cell_2_2.setCellValue("时间：" + begin + "至" + end); // 日期范围
        cell_4_3.setCellValue(businessData.getTurnover());// 营业额
        cell_5_3.setCellValue(businessData.getValidOrderCount()); // 有效订单
        cell_4_5.setCellValue(businessData.getOrderCompletionRate()); // 订单完成率
        cell_4_7.setCellValue(businessData.getNewUsers());  // 新增用户数
        cell_5_5.setCellValue(businessData.getUnitPrice()); // 平均客单价

        // 填充明细数据
        for (int i = 0; i < 30; i++) {
            // 查询某天对应的运营数据
            BusinessDataVO data = workspaceService.getBusinessData(LocalDateTime.of(begin, LocalTime.MIN), LocalDateTime.of(begin, LocalTime.MAX));
            // 获取行
            XSSFRow row8 = sheet1.getRow(7 + i);
            // 获取单元格并填充数据
            row8.getCell(1).setCellValue(begin.toString()); // 填充日期
            row8.getCell(2).setCellValue(data.getTurnover()); // 填充营业额
            row8.getCell(3).setCellValue(data.getValidOrderCount()); // 填充有效订单
            row8.getCell(4).setCellValue(data.getOrderCompletionRate()); // 填充订单完成率
            row8.getCell(5).setCellValue(data.getUnitPrice()); // 填充平均客单价
            row8.getCell(6).setCellValue(data.getNewUsers()); // 填充新增用户数
            begin = begin.plusDays(1);
        }
        // 3. 将准备好数据的工作簿返回
        return wb;
    }
}
