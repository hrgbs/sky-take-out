package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.mapper.EmployeeMapper;
import com.sky.service.EmployeeService;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeEditPasswordDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.exception.AccountLockedException;
import com.sky.exception.AccountNotFoundException;
import com.sky.exception.PasswordErrorException;
import com.sky.result.PageResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();

        //1、根据用户名查询数据库中的数据

        Employee employee = employeeMapper.getByUsername(username);

        //2、处理各种异常情况（用户名不存在、密码不对、账号被锁定）
        if (employee == null) {
            //账号不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //密码比对
        //   TODO 后期需要进行md5加密，然后再进行比对
        //对参数使用MD5进行加密
       // password = DigestUtils.md5DigestAsHex(password.getBytes());

        //对参数使用MD5+盐值对密码进行加密
        password = DigestUtils.md5DigestAsHex((password+employee.getSalt()).getBytes());

        if (!password.equals(employee.getPassword())) {
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        if (employee.getStatus() == StatusConstant.DISABLE) {
            //账号被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }


        //3、返回实体对象
        return employee;
    }

    /**
     * 新增员工
     *
     * @param employeeDTO
     */

    public void save(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();
        //使用对象属性拷贝 必须要求属性名一致
        BeanUtils.copyProperties(employeeDTO, employee);
        //增加其他属性
        employee.setStatus(StatusConstant.ENABLE);

     /*   employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());
        //从当前线程空间中取出存入的empID
        employee.setCreateUser(BaseContext.getCurrentId());
        employee.setUpdateUser(BaseContext.getCurrentId());*/

        //使用加盐加密
        String salt = UUID.randomUUID().toString().replace("-", "");
        String password = DigestUtils.md5DigestAsHex((PasswordConstant.DEFAULT_PASSWORD + salt).getBytes());
        employee.setSalt(salt);
        employee.setPassword(password);

        employeeMapper.insertEmp(employee);

    }

    /**
     * 分页查询
     *
     * @param employeePageQueryDTO
     * @return
     */

    @Override
    public PageResult pageQuery(EmployeePageQueryDTO employeePageQueryDTO) {

        PageHelper.startPage(employeePageQueryDTO.getPage(), employeePageQueryDTO.getPageSize());

        Page<Employee> page = employeeMapper.pageQuery(employeePageQueryDTO);

        long total = page.getTotal();
        List<Employee> records = page.getResult();

        return new PageResult(total, records);
    }

    /**
     * 启用禁用员工
     *
     * @param status
     * @param id
     */

    public void startOrStop(Integer status, long id) {
        // 需要达到可以根据需要修改的属性来修改目标员工，所有需要将员工的所有信息都传进来
        //创建一个员工对象接收传入的需要修改的目标属性和查找目标员工的属性id
     /*  创建一个新的员工对象来接收传入参数
        Employee employee = new Employee();
        employee.setId(id);
        employee.setStatus(status);*/

        //通过builder来获得构建器对象
        Employee employee = Employee.builder()
                .status(status)
                .id(id)
                .build();
        employeeMapper.update(employee);
    }

    /**
     * 根据id查询员工
     *
     * @return
     */
    public Employee getById(long id) {
        Employee employee = employeeMapper.getById(id);
        employee.setPassword("*******");
        return employee;
    }

    /**
     * 修改员工信息
     */

    public void update(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDTO, employee);

      /*  employee.setUpdateTime(LocalDateTime.now());
        employee.setUpdateUser(BaseContext.getCurrentId());*/

        employeeMapper.update(employee);
    }


    /**
     * 修改密码
     * @param employeeEditPasswordDTO
     */
    public void editPassword(EmployeeEditPasswordDTO employeeEditPasswordDTO) {
        //
        Long currentId = BaseContext.getCurrentId();
        String oldpassword = employeeEditPasswordDTO.getOldPassword();
        String newpassword = employeeEditPasswordDTO.getNewPassword();
        if (oldpassword !=null && newpassword !=null && oldpassword != newpassword) {
        //新密码加盐加密
        String salt = UUID.randomUUID().toString().replace("-", "");
        String password = DigestUtils.md5DigestAsHex((newpassword + salt).getBytes());
        employeeMapper.editPassword(currentId,password);
        return;
        }
        throw new AccountNotFoundException(MessageConstant.PASSWORD_EDIT_FAILED);

    }

}
