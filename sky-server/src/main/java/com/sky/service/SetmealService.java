package com.sky.service;


import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public interface SetmealService  {


    /**
     * 新增套餐
     * @param setmealDTO
     */
    void saveWithDishes(SetmealDTO setmealDTO);

    /**
     * 套餐分页查询
     * @param setmealPageQueryDTO
     * @return
     */

    PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO);

    /**
     * 修改套餐
     * @param setmealDTO
     */
    void updateWithDishs(SetmealDTO setmealDTO);

    /**
     *
     * @param status
     * @param id
     */
    void startOrStop(Integer status, Long id);


    /**
     * 根据id查询套餐
     * @param id
     */

    SetmealVO getById(Long id);


    /**
     * 根据ids删除套餐
     * @param ids
     */
    void deleteBatch(List<Long> ids);


    /**
     * 条件查询
     * @param setmeal
     * @return
     */
    List<Setmeal> list(Setmeal setmeal);

    /**
     * 根据id查询菜品选项
     * @param id
     * @return
     */
    List<DishItemVO> getDishItemById(Long id);
}
