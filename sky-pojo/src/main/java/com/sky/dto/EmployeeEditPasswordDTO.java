package com.sky.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeEditPasswordDTO implements Serializable {

    //员工id
    private Long id;

    //旧密码
    private String oldPassword;

    //新密码
    private String newPassword;

}
