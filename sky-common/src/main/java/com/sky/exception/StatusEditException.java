package com.sky.exception;

public class StatusEditException extends BaseException {

    public StatusEditException() {
    }

    public StatusEditException(String msg) {
        super(msg);
    }

}
